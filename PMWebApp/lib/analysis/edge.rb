module Analysis
  class Edge
    attr_reader :clazz, :covering, :error, :precision

    def initialize(edge_ptr, features)
      return if edge_ptr.nil? || edge_ptr.null?

      @features = features

      edge = PMAlgo::EdgesT.new(edge_ptr)

      @clazz = edge[:nclass]
      @covering = edge[:cov]
      @error = edge[:err]
      @precision = @covering / @error
      @precision = 0 if @error == 0

      extract_rules_from_ptr(edge)
    end

    def rule_str
      str = ''
      first = true
      @rules.each do |f, minmax|
        if first
          first = false
        else
          str += ' AND '
        end
        str += "(#{@features[f]} >= #{minmax.first} AND #{@features[f]} <= #{minmax.last})"
      end
      str += "    => #{@clazz}"
    end

    def to_s
      "class=#{@clazz},cov=#{@covering},err=#{@error},prec=#{@precision}"
    end

    private

    def extract_rules_from_ptr(edge)
      @rules = {}
      comb_array = FFI::Pointer.new(:int, edge[:comb])
      min_array = FFI::Pointer.new(PMAlgo::Elem32T, edge[:min])
      max_array = FFI::Pointer.new(PMAlgo::Elem32T, edge[:max])

      len = edge[:ndim]
      (0...len).each do |i|
        feature = comb_array[i].read_array_of_type(:int32, :read_int32, 1).first
        min = PMAlgo::Elem32T.new(min_array[i])
        max = PMAlgo::Elem32T.new(max_array[i])
        @rules[feature] = [min[:i], max[:i]]
      end
    end
  end
end
