require 'ffi'

module TestLib
  extend FFI::Library
  ffi_lib "#{File.expand_path File.dirname(__FILE__)}/testlib.so"
  attach_function :calculate_something, [:int, :float], :double
end
