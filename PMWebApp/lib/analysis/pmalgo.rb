require 'ffi'

module PMAlgo
  PMALGO_LIB_NAME = 'libTestCLibrary.so'
  # PMALGO_LIB_NAME = 'libpmc-api.so'
  extend FFI::Library
  ffi_lib "#{File.expand_path File.dirname(__FILE__)}/#{PMALGO_LIB_NAME}"


  # extern struct edges_collector_t *pmc_api_learn_model(char *data_buf,
  #                                                      double validperc,
  #                                                      double cov,
  #                                                      double maxerrperc,
  #                                                      const double mincov,
  #                                                      const size_t ncondmax,
  #                                                      const size_t ncondmin,
  #                                                      const char autodim)
  attach_function :pmc_api_learn_model, [:string, :double, :double, :double, :double, :int, :int, :char], :pointer

  # struct edges_collector_t {
  #   size_t len;
  #   size_t maxlen;
  #   struct edges_t *ed[1];
  # };
  class EdgesCollectorT < FFI::ManagedStruct
    layout :len, :int,
           :maxlen, :int,
           :ed, :pointer

    def self.release(ptr)
      0...self[:len].each { |i| self[:ed][i].free }
      ptr.free
    end
  end

  # struct edges_t {
  #   size_t ndim;
  #   double cov;
  #   double err;
  #   /* enum data_type_t *type; */
  #   size_t *comb;
  #   union elem_32_t *max;
  #   union elem_32_t *min;
  #   int32_t nclass;
  # };
  class EdgesT < FFI::ManagedStruct
    layout :ndim, :int,
           :cov, :double,
           :err, :double,
           :comb, :pointer,
           :max, :pointer,
           :min, :pointer,
           :nclass, :int

    def self.release(ptr)
      0...self[:ndim].each do |i|
        self[:comb][i].free
        self[:min][i].free
        self[:max][i].free
      end
      ptr.free
    end
  end

  # union elem_32_t {
  #   /** Integer data. */
  #   int32_t i;
  #   /** Float data. */
  #   float f;
  #   /** Encoded float data. */
  #   encoded_float_t e;
  # };
  class Elem32T < FFI::Union
    layout :i, :int,
           :f, :float
  end
end
