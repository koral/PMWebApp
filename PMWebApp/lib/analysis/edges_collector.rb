module Analysis
  class EdgesCollector
    attr_reader :edges

    def initialize(raw_result, features)
      return  if raw_result.nil? || raw_result.null?
      @features = features
      edges_collector = PMAlgo::EdgesCollectorT.new(raw_result)
      init_edges(edges_collector)
    end

    def init_edges(collector)
      @edges = []
      @len = collector[:len]
      @maxlen = collector[:maxlen]

      val_array = FFI::Pointer.new(PMAlgo::EdgesT, collector[:ed])
      0.upto(@len - 1).each do |i|
        edge = val_array[i]
        @edges << Analysis::Edge.new(edge, @features)
      end
    end

    def rules_str
      rules = []
      @edges.each do |edge|
        rules << edge.rule_str
      end
      rules
    end

    def to_s
      str = ''
      @edges.each do |edge|
        str += "(len=#{@len}, maxlen=#{@maxlen}, (#{edge})),"
      end
      str
    end
  end
end
