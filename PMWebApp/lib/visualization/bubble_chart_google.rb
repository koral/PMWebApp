module Visualization
  class BubbleChartGoogle
    attr_reader :chart, :rules_str

    def initialize(result)
      @result = result
      @data_table = data_table(result)
      @options = options
      @chart = GoogleVisualr::Interactive::BubbleChart.new(@data_table, @options)
    end

    def rules_str
      @result.rules_str
    end

    private

    def options
      {
          :width => 800, :height => 500,
          :title => 'PMAlgo result visualization',
          :hAxis => {:title => 'Precision'},
          :vAxis => {:title => 'Error'},
          :bubble => {:textStyle => {:fontSize => 11}}
      }
    end

    def data_table(result)
      data_table = GoogleVisualr::DataTable.new

      data_table.new_column('string', 'ID')
      data_table.new_column('number', 'Precision')
      data_table.new_column('number', 'Error')
      data_table.new_column('string', 'Class')
      data_table.new_column('number', 'Covering')

      i = 0
      result.edges.each do | edge |
        row = []
        row << "R#{i}"
        row << edge.precision
        row << edge.error
        row << edge.clazz.to_s
        row << edge.covering
        data_table.add_row(row)

        i += 1
      end

      data_table
    end
  end
end