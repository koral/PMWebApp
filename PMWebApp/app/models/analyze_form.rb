module DynamicSetter
  def make_num_setter(*names)
    names.each do |name|
      define_method("#{name}=") do |val|
        val = val.to_i if val.instance_of? String
        instance_variable_set("@#{name}", val)
      end
    end
  end

  def make_bool_setter(*names)
    names.each do |name|
      define_method("#{name}=") do |val|
        val = val.to_i if val.instance_of? String
        instance_variable_set("@#{name}", val)
      end
    end
  end
end

class AnalyzeForm
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  extend ActiveModel::Callbacks
  extend DynamicSetter

  attr_reader :file, :validperc, :cov, :maxerrperc, :mincov, :ncondmax, :ncondmin, :autodim, :file_data

  validates_presence_of :file, :validperc, :cov, :maxerrperc, :mincov, :ncondmax, :ncondmin
  validates_numericality_of :validperc, :cov, :maxerrperc, :mincov, :ncondmax, :ncondmin

  validates :validperc, inclusion: 0..100
  validates :cov, inclusion: 0..100
  validates :maxerrperc, inclusion: 0..100
  validates :mincov, inclusion: 0..100
  validates :ncondmax, inclusion: 0..100
  validates :ncondmin, inclusion: 0..100

  validates :autodim, inclusion: [0, 1]

  make_num_setter :validperc, :cov, :maxerrperc, :mincov, :ncondmax, :ncondmin, :autodim

  def initialize(attrs = {})
    attrs.each do |name, val|
      send("#{name}=", val)
    end
  end

  def set_defaults!
    @validperc = 0
    @cov = 20
    @maxerrperc = 0
    @mincov = 5
    @ncondmax = 10
    @ncondmin = 1
    @autodim = 0
  end

  def file=(file)
    @file = file
    @file_data = ''
    @file_data = @file.read if @file
  end

  def features_from_file
    return [] unless @file
    mat = @file_data.split("\n")
    mat[1].split("\t")
  end
end
