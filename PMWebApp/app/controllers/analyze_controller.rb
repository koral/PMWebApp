require 'analysis/pmalgo'
require 'analysis/edges_collector'

class AnalyzeController < ApplicationController
  before_action :set_form, only: [:default, :do]

  def default
  end

  def do
    unless @form.valid?
      flash[:error] = @form.errors.full_messages.join('<br>').html_safe
      render action: 'default'
      return
    end

    raw_result = PMAlgo.pmc_api_learn_model(@form.file_data,
                                            @form.validperc,
                                            @form.cov,
                                            @form.maxerrperc,
                                            @form.mincov,
                                            @form.ncondmax,
                                            @form.ncondmin,
                                            @form.autodim)

    result = Analysis::EdgesCollector.new(raw_result, @form.features_from_file)
    # raw_result.free
    @result = result
    bubble_chart = Visualization::BubbleChartGoogle.new(result)
    @chart = bubble_chart.chart
    @rules = bubble_chart.rules_str
  end

  private

  def set_form
    if params[:analyze_form]
      @form = AnalyzeForm.new(params[:analyze_form])
    else
      @form = AnalyzeForm.new
      @form.set_defaults!
    end
  end
end
