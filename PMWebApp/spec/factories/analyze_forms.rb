require 'analyze_form'
require 'tempfile'

FactoryGirl.define do
  factory :analyze_form do |f|
    f.file do
      file = Rack::Test::UploadedFile.new('spec/data/2dchess_1000p_1000v_10dim_0err.float')
    end
    f.validperc 0
    f.cov 20
    f.maxerrperc 0
    f.mincov 5
    f.ncondmax 10
    f.ncondmin 1
    f.autodim 0
  end
end