require 'rails_helper'

RSpec.describe AnalyzeController, :type => :controller do
  describe "GET #default" do
    it 'renders form' do
      get :default
      response.should render_template :default
    end
  end

  describe "POST #do" do
    context 'with valid attributes' do
      it 'renders :do view' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form)
        response.should render_template :do
      end

    end

    context 'with invalid attributes' do
      it 'stays on :default when no file chosen' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, file: nil)
        response.should render_template :default
      end

      it 'stays on :default when valid perc invalid' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, validperc: -1)
        response.should render_template :default
      end

      it 'stays on :default when valid perc not chosen' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, validperc: nil)
        response.should render_template :default
        end

      it 'stays on :default when cov invalid' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, cov: -1)
        response.should render_template :default
      end

      it 'stays on :default when cov not chosen' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, cov: nil)
        response.should render_template :default
      end

      it 'stays on :default when mincov invalid' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, mincov: -1)
        response.should render_template :default
      end

      it 'stays on :default when mincov not chosen' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, mincov: nil)
        response.should render_template :default
      end

      it 'stays on :default when ncondmin invalid' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, ncondmin: -1)
        response.should render_template :default
      end

      it 'stays on :default when ncondmin not chosen' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, ncondmin: nil)
        response.should render_template :default
      end

      it 'stays on :default when ncondmax invalid' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, ncondmax: -1)
        response.should render_template :default
      end

      it 'stays on :default when ncondmax not chosen' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, ncondmax: nil)
        response.should render_template :default
      end

      it 'stays on :default when autodim invalid' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, autodim: -1)
        response.should render_template :default
      end

      it 'stays on :default when autodim not chosen' do
        post :do, analyze_form: FactoryGirl.attributes_for(:analyze_form, autodim: nil)
        response.should render_template :default
      end
    end
  end
end