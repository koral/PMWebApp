require 'analysis/edge'
require 'analysis/pmalgo'
require 'ffi'

describe 'Edge' do
  subject { Analysis::Edge.new(nil, []) }

  it 'has class' do
    subject.respond_to? :clazz
    end

  it 'has rule' do
    subject.respond_to? :rule
  end

  it 'has error' do
    subject.respond_to? :error
  end

  it 'has precision' do
    subject.respond_to? :precision
  end

  it 'responds to rule_str' do
    subject.respond_to? :rule_str
  end
end
