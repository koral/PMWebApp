require 'analysis/edges_collector'

describe 'EdgesCollector' do
  subject { Analysis::EdgesCollector.new(nil, []) }

  it 'has edges' do
    subject.respond_to? :edges
  end

  it 'responds to rules_str' do
    subject.respond_to? :rules_str
  end
end