require 'analysis/pmalgo'
require 'ffi/pointer'

describe 'PMAlgo' do
  before :each do
    @data = File.new('spec/data/2dchess_1000p_1000v_10dim_0err.float', 'r')
    @data = @data.read()
  end

  it 'has api method' do
    expect(PMAlgo.respond_to? :pmc_api_learn_model).to eq true
  end

  it 'calls api without crash' do
    PMAlgo.pmc_api_learn_model(@data, 0, 20, 0, 5, 10, 1, 0)
  end

  describe 'returns pointer' do
    before :each do
      @model = PMAlgo.pmc_api_learn_model(@data, 0, 20, 0, 5, 10, 1, 0)
    end

    it '' do
      expect(@model).to be_a FFI::Pointer
    end

    it 'which is not null' do
      expect(@model.null?).to eq false
    end


    context 'which points to EdgesCollectorT' do
      before :each do
        @result = PMAlgo::EdgesCollectorT.new(@model)
      end

      it do
        result = PMAlgo::EdgesCollectorT.new(@model)
        expect(result[:len].nil?).to eq false
        expect(result[:maxlen].nil?).to eq false
        expect(result[:ed].nil?).to eq false
      end

      context 'which contains edges' do
        before :each do
          @ed = @result[:ed]
        end

        it 'pointer' do
          expect(@ed[0]).to be_a FFI::Pointer
        end

        it 'pointing to EdgesT' do
          edges = FFI::Pointer.new(PMAlgo::EdgesT, @ed[0])
          edge = PMAlgo::EdgesT.new(edges[0])
          expect(edge[:ndim].nil?).to eq false
          expect(edge[:cov].nil?).to eq false
          expect(edge[:err].nil?).to eq false
          expect(edge[:max].nil?).to eq false
          expect(edge[:min].nil?).to eq false
          expect(edge[:nclass].nil?).to eq false
        end
      end
    end
  end
end