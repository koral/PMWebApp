require 'spec_helper'

describe 'analyze routes', type: :routing do
  it 'GETs to analyze#default from main URL' do
    expect(:get => '/').to route_to(controller: 'analyze', action: 'default')
  end

  it 'does not GET to /analyze/default' do
    expect(:get => '/analyze').not_to be_routable
  end

  it 'does not POST to /analyze/do' do
    expect(:post => '/').not_to be_routable
  end

  it 'does not GET /visualization' do
    expect(:get => '/visualization').not_to be_routable
  end
end