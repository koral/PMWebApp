require 'spec_helper'
require 'analyze_form'
require 'factory_girl'

describe AnalyzeForm do
  it 'has valid factory' do
    FactoryGirl.build(:analyze_form).should be_valid
  end

  it 'is invalid if file is nil' do
    FactoryGirl.build(:analyze_form, file: nil).should_not be_valid
  end

  it 'is invalid if validperc is nil' do
    FactoryGirl.build(:analyze_form, validperc: nil).should_not be_valid
  end

  it 'is invalid if validperc is not in 0..100' do
    FactoryGirl.build(:analyze_form, validperc: -1).should_not be_valid
    FactoryGirl.build(:analyze_form, validperc: -10).should_not be_valid
    FactoryGirl.build(:analyze_form, validperc: 101).should_not be_valid
    FactoryGirl.build(:analyze_form, validperc: 110).should_not be_valid
  end

  it 'is invalid if maxerrperc is nil' do
    FactoryGirl.build(:analyze_form, maxerrperc: nil).should_not be_valid
  end

  it 'is invalid if maxerrperc is not in 0..100' do
    FactoryGirl.build(:analyze_form, maxerrperc: -1).should_not be_valid
    FactoryGirl.build(:analyze_form, maxerrperc: -10).should_not be_valid
    FactoryGirl.build(:analyze_form, maxerrperc: 101).should_not be_valid
    FactoryGirl.build(:analyze_form, maxerrperc: 110).should_not be_valid
  end

  it 'is invalid if mincov is nil' do
    FactoryGirl.build(:analyze_form, mincov: nil).should_not be_valid
  end

  it 'is invalid if mincov is not in 0..100' do
    FactoryGirl.build(:analyze_form, mincov: -1).should_not be_valid
    FactoryGirl.build(:analyze_form, mincov: -10).should_not be_valid
    FactoryGirl.build(:analyze_form, mincov: 101).should_not be_valid
    FactoryGirl.build(:analyze_form, mincov: 110).should_not be_valid
  end

  it 'is invalid if ncondmax is nil' do
    FactoryGirl.build(:analyze_form, ncondmax: nil).should_not be_valid
  end

  it 'is invalid if ncondmax is not in 0..100' do
    FactoryGirl.build(:analyze_form, ncondmax: -1).should_not be_valid
    FactoryGirl.build(:analyze_form, ncondmax: -10).should_not be_valid
    FactoryGirl.build(:analyze_form, ncondmax: 101).should_not be_valid
    FactoryGirl.build(:analyze_form, ncondmax: 110).should_not be_valid
  end

  it 'is invalid if ncondmin is nil' do
    FactoryGirl.build(:analyze_form, ncondmin: nil).should_not be_valid
  end

  it 'is invalid if ncondmin is not in 0..100' do
    FactoryGirl.build(:analyze_form, ncondmin: -1).should_not be_valid
    FactoryGirl.build(:analyze_form, ncondmin: -10).should_not be_valid
    FactoryGirl.build(:analyze_form, ncondmin: 101).should_not be_valid
    FactoryGirl.build(:analyze_form, ncondmin: 110).should_not be_valid
  end

  it 'is invalid if autodim is not in [0, 1]' do
    FactoryGirl.build(:analyze_form, autodim: false).should_not be_valid
    FactoryGirl.build(:analyze_form, autodim: true).should_not be_valid
    FactoryGirl.build(:analyze_form, autodim: 0.5).should_not be_valid
    FactoryGirl.build(:analyze_form, autodim: -5).should_not be_valid
  end

  it 'is valid if setter recieves valid String' do
    FactoryGirl.build(:analyze_form, validperc: '50').should be_valid
    FactoryGirl.build(:analyze_form, maxerrperc: '50').should be_valid
    FactoryGirl.build(:analyze_form, mincov: '50').should be_valid
    FactoryGirl.build(:analyze_form, ncondmax: '50').should be_valid
    FactoryGirl.build(:analyze_form, ncondmin: '50').should be_valid
    FactoryGirl.build(:analyze_form, autodim: 'false').should be_valid
    FactoryGirl.build(:analyze_form, autodim: 'true').should be_valid
  end
end