require 'rails_helper'

describe 'AnalyzeForm' do
  context 'default input' do
    it 'shows chart and rules' do
      visit root_url

      attach_file 'analyze_form_file', 'spec/data/2dchess_1000p_1000v_10dim_0err.float'
      click_button 'Find Rules!'

      page.should have_content 'R0)'

      within 'h1' do
        page.should have_content 'Visualization of Result'
      end
    end
  end
end